# SteamWeb Api#

This is an overview of the Steam Web API used in the Steam iOS and Android Apps, as well as on [Steam Chat](http://steamcommunity.com/chat)

## Preface ##

Steam's Web API consists mostly of long-polled POST requests, although some commands require GET requests. 

Requests made via POST require the request data to be passed as URLEncoded POST variables, and is analogous to GET

All responses are JSON encoded, and vary in format 


## Authentication ##

Logging in on the Steam Android/iOS App is normally consists of the following steps

* GET request to https://steamcommunity.com/mobilelogin via a hooked browser control 
* The user types in their credentials, and submits them
* Embedded JavaScript on the page makes additional requests and returns an auth key, a steamguard error, or a general error.
* The hooked browser returns this information to the application via cookie sets, where it can be further used.

It is possible, however to hard code the logic embedded within the page

### Request a RSA Key ###
**Request:** 

POST https://steamcommunity.com/mobilelogin/getrsakey/

**Parameters:**

```text
username:	[Required, STR] The plain-text username of the local user.
donotcache:	[Optional, STR] The current time in milliseconds. Ignored by server; Only used for internet explorer AJAX requests (I think).
```
**Response:**

```text
success:		[Required, BOOL] Indicates whether or not the request succeded
publickey_mod:	[Required, STR] A Hex-encoded RSA public key modulus
publickey_exp:	[Required, STR] A Hex-encoded RSA public key exponent
timestamp: 		[Required, INT] The UNIX timestamp of when the request was processed.
token_gid: 		[Required, STR] unknown
```
You must keep the modulus, exponent, and timesamp, as they are used later.

### Request Login ###
In order to log into the service, you must first:

* Encrypt the user's password using RSA with the exponent and modulus returned in the first section
* Determine which steam universe you wish to use
* If a previous login failed because of steamguard, CAPTCHA, or two-factor authentication, you must send the relevant values here.

In order to encrypt the password, you must set up a RSA Cryptosystem with the Modulus, Exponent, and you **MUST** use PKCS1,* not* OAEP

The following is an excerpt from a C# project that properly encodes the password.


```
#!c#
RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();

RSA.ImportParameters(new RSAParameters
{
	Modulus = HexToBytes(Result.publickey_mod),
	Exponent = HexToBytes(Result.publickey_exp)
});
string EncryptedPassword = Convert.ToBase64String(RSA.Encrypt(Encoding.UTF8.GetBytes("MyPasswordHere"), false));
```

once the password has been properly encoded, a steam universe must be chosen which determines which set of base URLs you will use after login.
You can choose between Public, Beta, and an Internal/Alpha universe.

* Public: DE45CD61
* Private: 7DC60112
* Alpha: E77327FA

The default and best choice is public, which is what will be used here.

**Request:**

POST https://steamcommunity.com/mobilelogin/dologin/

**Parameters:**

```text
password:			[Required, STR] Base64-encoded RSA encrypted password of the local user
username:			[Required, STR] Plain-text username of the local user
rsatimestamp:		[Required, INT] The timestamp returned by the RSA Key request above.
oauth_client_id:	[Required, STR] The Steam Universe value chosen above.
twofactorcode:		[Optional, STR] The two factor authorization code for the user. Only required if a previous login attempt failed with requires_twofactor set to true
emailauth:			[Optional, STR] The steamguard code for the user. Only required if a previous login attempt failed with requires_emailauth set to true
loginfriendlyname:	[Optional, STR] A web-safe name for the client. The original value is "Steam Mobile Client"
CAPTCHAgid: 		[Optional, STR] The GID of the CAPTCHA that was requested. Only required if a previous login attempt failed with CAPTCHA_needed or bad_CAPTCHA set to true
CAPTCHA_text: 		[Optional, STR] The text of the CAPTCHA that was requested. Only required if a previous login attempt failed with CAPTCHA_needed or bad_CAPTCHA set to true
remember_login: 	[Optional, BOOL] Indicates whether the server will attempt to set a cookie to remember the user.
donotcache: 		[Optional, STR] The current time in milliseconds. Ignored by server; Only used for internet explorer AJAX requests (I think).
```

**Response:**
```text
success:			[Required, BOOL] Indicates whether the authentication request succeeded or not.
login_complete: 	[Required, BOOL] Indicates whether the authentication request completed, or additional authorization information is required.
requires_twofactor: [Required, BOOL] Indicates whether or not the login must be resubmitted with a twofactor code
emailauth_needed: 	[Required, BOOL] Indicates whether or not the login must be resubmitted with a email code
CAPTCHA_needed:		[Required, BOOL] Indicates whether or not the login must be resubmitted with a CAPTCHA code. The image is generated from the GID
bad_CAPTCHA:		[Required, BOOL] Indicates whether or not the previously tried CAPTCHA was invalid.
CAPTCHA_gid:		[Optional, STR]	The GID for the CAPTCHA to be submitted on the next login attempt. This must be submitted along with the CAPTCHA text for verification.
message: 			[Optional, STR] The type of email authentication required. Usually the value is "steamguard", but could also indicate the twofactor type.
emaildomain: 		[Optional, STR] The domain of the email that the authorization/steamguard message was sent to
emailsteamid: 		[Optional, STR] The SteamID of the account the email was generated for.
redirect_uri: 		[Optional, STR] Specifically used inside the android/iOS app to indicate that the login was successful. This redirects the hooked browser to custom code to read the result.
oauth: 				[Optional, OBJ] OAuth information container. The oauth_token must be persisted in order to skip the login process.
{
	steamid:		[Required, STR] The user's SteamID
	oauth_token:	[Required, STR] The OAuth token used for repeat authentication, and all secure requests. MUST BE PERSISTANT.
	webcookie:		[Required, STR] Cookie used to maintain secure access to steam's normal services (store, profile, settings, etc). This is to be set whenever a steam page is loaded.
}
```

CAPTCHA images can be generated via https://steamcommunity.com/login/rendercaptcha/?gid=<GID_HERE>
The GID must be kept in memory until the login is resubmitted in order for the CAPTCHA to be verified.

## UMQ  Requests ##
Once the login succeeds, the values in the 'oauth' array must be saved on the disk, as well as in memory. The disk-saved version is used to skip the login process in the future, and the memory-saved version is used for all requests from this point on. 

Depending on the Universe you chose in the login command, you can chose from several base URLs for the commands. The options are:

* Secure: 
    * Public: https://api.steampowered.com
    * Beta: https://api-beta.steampowered.com
    * Alpha: https://vitaliy.valvesoftware.com:8283
* Insecure:
    * Public: http://api.steampowered.com
    * Beta: http://api-beta.steampowered.com
    * Alpha: http://vitaliy.valvesoftware.com:8282

It is **NOT RECOMMENDED** that the insecure versions be used for anything, as they have the potential to be monitored by a third party, and they have not been tested.
All secure versions use SSL/TLS, regardless of port.

The default universe is Public, and will be assumed.

### UMQ Authentication ###

**Request:**

POST <BASE_URL>/ISteamWebUserPresenceOAuth/Logon/v0001

**Parameters:**
```text
access_token:	[Required, STR] The OAuth_Token that was received during authentication
ui_mode:		[Optional, STR] The UI mode for this instance. The default is "web"
```

**Response:**
```text
umqid: 		[Required, STR] The UMQID for the user. This is used to poll for new messages, persona state changes, and to send messages.
message:	[Required, INT] The initial message ID to be used when polling the server or sending messages. This MUST be kept in memory.
steamid:	[Required, STR] The SteamID of the user who was authenticated.
```
If an error occurs because the access_token is invalid, a '503 Forbidden' HTTP error will be returned.
The UMQID returned by this request should only be stored temporarily, as it expires if not used.

Any requests beyond this point can be performed in any order.

### UMQ Poll ##
This request does most of the heavy lifting. You place a long poll request to it (the timeout is specified in the request), and it returns when data is available. 
Returned events include the following:

* Persona state change
    * Sign On
    * State Changes (Online/Away/Etc)
    * Current game (Not reliable)
    * Sign Off
* Typing Events
* Messages
    * Friend's messages
    * Your messages from other devices (Not reliable)
    * Emoticon messages (not sure why this is a distinct type)

Due to the unreliability of the persona state change messages to send game information, each time a state change is received, a request must be made to get additional user information. I have tested this with  **OAuth UserSummaries** however it may be possible to use **UMQ PollStatus**, however I haven't used the latter yet.
 
**Request:**

POST <BASE_URL>/ISteamWebUserPresenceOAuth/Poll/v0001

**Parameters:**
```text
access_token:	[Required, STR] The OAuth_Token that was received during authentication
umqid: 			[Required, STR] The UMQID that was received during UMQ Authentication
message: 		[Required, INT] The ID of the last message received
pollid: 		[Required, INT] The number of polls performed. This must start as 0 and be incremented each time a UMQ Poll request is made.
sectimeout: 	[Required, INT] The number of seconds the server will wait before returning a result. The recommended Minimum is 20 seconds.
secidletime: 	[Required, INT] The number of seconds the user has been idle. Probably used to determine when to change status to AFK
use_accountids: [Required, BOOL] In order to use this with OAuth UserSummaries, this MUST be set to 0. When set to 0, SteamIDs are used. Otherwise, AccountIDs are used.
```

**Response:**
```text
pollid:			[Required, INT] The pollid that this is a response to. This can be used for error checking, but should not be used otherwise.
messages:		[Required, ARR] An array message containers received during this poll request.
messagelast:	[Required, STR] The ID of this message. The value here should be used for the next UMQ Message (Poll, Chat, etc)
timestamp:		[Required, STR] A UNIX timestamp indicating when the request completed.
utc_timestamp:	[Required, STR] A UNIX timestamp indicating when the request completed.
messagebase: 	[Required, INT] The message ID this request was made with.
sectimeout: 	[Required, INT] The number of seconds the server waited before returning
error: 			[Required, STR] The error, if any, that occurred during the poll. The success value is "OK"
```

All messages have a common base, followed by data that varies based on their type.

**Common**
```text
type:			[Required, STR] The type of message
timestamp:		[Required, INT] A Unix timestamp indicating when the message was sent.
utc_timestamp:	[Required, INT] A Unix timestamp indicating when the message was sent.
steamid_from:	[Required, STR] The SteamID64 of the user
```
known formats based on values of 'type' are shown below:

**Persona State**

```text
type:			[Required, STR] "personastate"
status_flags:	[Required, INT] The status of the user. Unknown use, do not seem to change.
persona_state:	[Required, INT] The current state of the user
persona_name:	[Required, STR] The name of the user
```
Possible persona states are:
```text
0 => Offline
1 => Online
2 => Busy
3 => Away
4 => Snooze
5 => Looking To Trade
6 => Looking To Play
```
There is no state for "In-Game" - That state must be determined by requesting additional information, and checking to see if an AppID or gameextrainfo key is present.

**Typing Event**

```text
type:	[Required, STR] "typing"
text:	[Required, STR] ""
```

**Friend Message**

```text
type:	[Required, STR] "saytext"
text:	[Required, STR] The chat message sent by a friend
```

**Self Message**

```text
type:	[Required, STR] "my_saytext"
text:	[Required, STR] A string containing a chat message
```

**Friend Emoticon**

```text
type:	[Required, STR] "emote"
NOT USED/UNKNOWN
```

**Self Emoticon**

```text
type:	[Required, STR] "my_emote"
NOT USED/UNKNOWN
```

### UMQ PollStatus###

This request appears to be similar to **UMQ Poll** except that it takes a steamID + UMQID instead of an OAuth token + UMQID, and returns SteamIDs by default, instead of AccountIDs
Internally, this is labelled as the "insecure" poll, and is only used with an insecure base URL, however it can be used with both secure and insecure base URLs
**Request:**

POST <BASE_URL>/ISteamWebUserPresenceOAuth/PollStatus/v0001

**Parameters:**
```text
steamid:		[Required, STR] The SteamID of the requesting account (the application user)
umqid: 			[Required, STR] The UMQID that was received during UMQ Authentication
message: 		[Required, INT] The ID of the last message received
pollid: 		[Required, INT] The number of polls performed. This must start as 0 and be incremented each time a UMQ Poll request is made.
sectimeout: 	[Required, INT] The number of seconds the server will wait before returning a result. The recommended Minimum is 20 seconds.
secidletime:	[Required, INT] The number of seconds the user has been idle. Probably used to determine when to change status to AFK
```

**Response:**

Same response format as **UMQ Poll**

### UMQ Message ###

Sends a message of the specified type, to the specified user. Seems to follow the same formatting as message types in **UMQ Poll**.

**Request:**

POST <BASE_URL>/ISteamWebUserPresenceOAuth/Message/v0001

**Parameters:**
```text
access_token:	[Required, STR] The OAuth_Token that was received during authentication
umqid: 			[Required, STR] The UMQID that was received during UMQ Authentication
type:			[Required, STR] The type of message to be sent 
steamid_dst:	[Required, STR] The SteamID64 of the user to whom the message is being sent
```
Additional parameters need to be sent based on the message type. As far as I'm aware, this follows the same rules as the message types in **UMQ Poll**. The only one which I have tested is "saytext" which works exactly the same as the **UMQ Poll** version.

**Response:**
```text
utc_timestamp:	[Required, STR] A UNIX timestamp indicating when the request was processed
error:			[Required, STR] A text value indicating the result. The success value is "OK"
```

### UMQ DeviceInfo ###
*It is not possible to use this request. It appears to register the application with push notifications, which is an internal android service*

**Request:**

POST <BASE_URL>/ISteamWebUserPresenceOAuth/DeviceInfo/v0001

**Parameters:**
```text
access_token:	[Required, STR] The OAuth_Token that was received during authentication
umqid: 			[Required, STR] The UMQID that was received during UMQ Authentication
deviceid:		[Required, STR] The C2DM Registration ID. In Android, it is "GOOG:" with the C2DM Registration ID appended
lang: 			[Required, STR] The C2DM Registration Language
im_enable:		[Required, BOOL] The C2DM activation state
im_id:			[Required, STR] The C2DM IMID. Only used when im_enable is set to true
```

**Response:**
```text
Unknown
```

### UMQ ServerInfo ###
Returns the current server time as a unix timestamp, and as a string

**Request:**

POST <BASE_URL>/ISteamWebAPIUtil/GetServerInfo/v0001

**Parameters:**
```text
None
```

**Response:**
```text
servertime: 		[Required, INT] A UNIX timestamp indicating the current server time
servertimestring:	[Required, STR] The current server time in an expanded format. Ex: "Thu Sep 25 17:57:38 2014"
```

### UMQ Logoff ###

**Request:**

POST <BASE_URL>/ISteamWebUserPresenceOAuth/Logoff/v0001

**Parameters:**
```text
```

**Response:**
```text
```

## OAuth Requests ##

### OAuth FriendList ###

Used to get a list of friends for the local user. Does not return any status information.

**Request:**

GET <BASE_URL>/ISteamUserOAuth/GetFriendList/v0001

**Parameters:**
```text
access_token:	[Required, STR] The OAuth_Token that was received during authentication
steamid:		[Required, STR] The SteamID64 of the local user
```

**Response:**
```text
friends:	[Required, ARR] An array of friend information containers
{
	{
		steamid:		[Required, STR] The SteamID64 of this friend.
		relationship:	[Required, STR] The relationship of this user to the local user
		friend_since:	[Required, INT] A UNIX timestamp indicating the elapsed time since this friend was added.
	}
}
```

### OAuth UserSummaries ##

Used to get user status information based on the steamIDs passed, and their relationships to the local user.

**Request:**

GET <BASE_URL>/ISteamUserOAuth/GetUserSummaries/v0001

**Parameters:**
```text
access_token:	[Required, STR] The OAuth_Token that was received during authentication
steamids:		[Required, STR] A comma-separated list of SteamID64 values to be queried.
```

**Response:**
```text
players:	[Required, ARR] An array of player information containers
{
	{
		steamid:					[Required, STR] The SteamID64 of this player.
		communityvisibilitystate:	[Required, INT]	The visibility state of this player's profile. 1 = Private, 3 = Public
		profilestate:				[Required, INT] Unknown. Possibly a boolean value determining if the player's profile is set up. Have not encountered values other than 1.
		personaname:				[Required, STR] The display name for this player.
		lastlogoff:					[Required, INT] The UNIX time this player logged off
		profileurl:					[Required, STR] A URL to this player's steam profile page.
		avatar:						[Required, STR] A URL to the smallest version of this player's avatar.
		avatarmedium:				[Required, STR] A URL to the medium version of this player's avatar
		avatarfull:					[Required, STR] A URL to the large version of this player's avatar
		personastate:				[Required, INT] The current status of this player. Follows the rules outline in UMQ Poll
		timecreated:				[Required, INT] The UNIX time that this player's account was created.
		personastateflags:			[Required, INT] Unknown. Seems to be a bit field representing some values, including whether or not the player is on mobile. 0 = Normal, 512 = Mobile. 
		realname:					[Optional, STR] The "real" name of this player.
		primaryclanid:				[Optional, STR] A SteamID64 for the primary group of this player
		gameserverip:				[Optional, STR]	The IP address of the server for the game being played
		gameserversteamid:			[Optional, STR]	The SteamID64 of the server for the game being played.
		gameid:						[Optional, STR] The AppID of the game being played. Is not always present; sometimes requires the gameextrainfo to be used.
		gameextrainfo:				[Optional, STR] The Name of the game being played. Is not always present; sometimes requires a lookup using the gameID
		loccountrycode:				[Optional, STR] The country code for this player
		locstatecode:				[Optional, STR] The state code for this player
		loccityid:					[Optional, STR] The city code for this player
	}
}
```

### OAuth GroupList ###

**Request:**

GET <BASE_URL>/ISteamUserOAuth/GetGroupList/v0001

**Parameters:**
```text
access_token:	[Required, STR] The OAuth_Token that was received during authentication
steamid:		[Required, STR] The SteamID64 of the local user
```

**Response:**
```text
```

### OAUTH GroupSummaries ###

**Request:**

GET  <BASE_URL>/ISteamUserOAuth/GetGroupSummaries/v0001

**Parameters:**
```text
access_token:	[Required, STR] The OAuth_Token that was received during authentication
steamids:		[Required, STR] A comma-separated list of SteamID64 values to be queried.
```

**Response:**
```text
```

### OAUTH AppInfo ###

**Request:**

GET <BASE_URL>/ISteamGameOAuth/GetAppInfo/v0001

**Parameters:**
```text
access_token:	[Required, STR] The OAuth_Token that was received during authentication
appids:			[Required, STR] A comma-separated list of AppID values to be queried.
```

**Response:**
```text
```